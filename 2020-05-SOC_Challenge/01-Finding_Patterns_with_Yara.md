# Finding Patterns with Yara

## Tasks

#### Instructions
> It is early 2017 and there is chatter on security forums about new malware spreading around like wildfire. You have gotten hold of some samples of the malware. Your goal is to analyze it and prepare yourself to be able to detect it in the future.

---
### Strings
> The samples are located in the `/home/student/Desktop/sample` directory and aptly named `1`, `2` and `3`.
>
> Perform string analysis on the files and try to look for rare strings that might be unique to the malware.


1. **How many sequences of printable characters with the minimum length of 7 are there in the file `sample/1`? Assume printable characters to be single-7-bit-byte characters (ASCII, ISO 8859, etc.), including tab and space characters but no other whitespace characters.**
> `2517`

The first time I did solved this question, I used the following command: `strings 1 | grep -E '^[[:alnum:][:blank:][:punct:]]{7,}' | wc -l`. It wasn't until I was speaking with some other people who had also completed this challenge used the following command: `strings -n 7 1 | wc -l`


---
### Simple Rule
> During the analysis, you noticed that there was an interesting string cmd.exe /c "%s". You want to detect this in the other sample files with a Yara rule.

###### Criteria
* Write a Yara rule to detect the presence of the string cmd.exe /c "%s".
* Save the rule into the /home/student/Desktop/rules/offset.yar file.


This is an autograded question which is marked as complete once a rule is written which meets the requirements.

I used the [writing rules](https://yara.readthedocs.io/en/stable/writingrules.htm) page from yara to write this simple rule.

```
rule command
{
    strings:
        $CMD = "cmd.exe /c \"%s\""
    condition:
        $CMD
}
```


---
### Offset
> Now it's time to test your new Yara rule with the `sample/2` file.

1. **What is the offset of the string cmd.exe /c "%s" in the file sample/2? Provide an answer in the hex form prepended by 0x e.g. 0x1234.**
> `0xf42c`

Running the command from the `rules` directory gives the answer above:
```bash
yara offset.yar ../sample/2 -s
command ../sample/2
0xf42c:$cmd: cmd.exe /c "%s"
```


---
### Filetypes
> Your colleagues from the IT department have collected a number of suspicious files and placed a copy of them into the Desktop/suspicious directory.
>
> You need to check how many of the files are Windows executables (PE file format), but there are too many files to analyze manually. You can leverage the power of Yara to simplify your work.

###### Criteria
* Write a Yara rule capable of detecting files that have a **PE file format** in the `/home/student/Desktop/suspicious` directory.
* Save the rule into the `/home/student/Desktop/rules/pe.yar` file.

This is another autograded task where you need to write a rule which detects files which are Windows PE executables.

The resources I used for this one are:
* [Magic Bytes Help](https://www.netspi.com/blog/technical/web-application-penetration-testing/magic-bytes-identifying-common-file-formats-at-a-glance/)
* [String offsets](https://yara.readthedocs.io/en/stable/writingrules.html#string-offsets-or-virtual-addresses)

The rule:
```
rule my_pe
{
    strings:
        $MZ = { 4D 5A }
        $PE = { 50 45 00 00 }
    condition:
        $MZ at 0x0 and $PE
}
```


---
### Detect the Malware
> You have gotten a list of strings commonly present in the malware from a threat intelligence community online. These strings are found in the file Desktop/intel/strings.txt.
>
> Create a Yara rule out of these strings to detect the malware. You can also leverage the knowledge from previous sample analysis. Figure out how many files in the Desktop/suspicious directory are this malware.
>
> Create the rule file into the Desktop/rules directory and name the file malware.yar.

###### Criteria
* Write a Yara rule capable of detecting files that are actually malware in the `/home/student/Desktop/suspicious` directory.
    * Create the Yara rule from the strings in `/home/student/Desktop/intel/strings.txt` to detect the malware.
* Save the rule into the `/home/student/Desktop/rules/malware.yar` file.

The last task in this is yet another autograded one, as soon as you generate the rule which meets the criteria, it auto-completes.

The script I wrote to generate the rule looks like this:
```bash
#!/bin/bash

FILENAME="/home/student/Desktop/intel/strings.txt"
COUNTER=0
OUTFILE="malware.yar"

cat << EOF > "${OUTFILE}"
rule mally
{
    strings:
        \$PE = { 50 45 00 00 }
        \$MZ = { 4D 5A }
EOF

while read LINE
do
    echo "        \$str${COUNTER} = \"${LINE}\"" >> "${OUTFILE}"
    COUNTER=$((COUNTER + 1))
done < "${FILENAME}"

cat << EOF >> "${OUTFILE}"
    condition:
        200 of (\$str*) and \$PE and \$MZ
}
EOF
```

Unfortunately, this doesn't escape the quotation marks, so there was some manual editing by running yara and seeing which line it failed to parse. Once I had that sorted, I had to fine tune the condition. I initially started with `all of ($str*)`, but this apparently was too restrictive. I took it down to `100 of ($str*)`, but that was too loose. I settled on **200** and that popped the final check to complete this challange.

After some researching, I could swap out `${LINE}` for `${LINE//\"/\\\"}`.
