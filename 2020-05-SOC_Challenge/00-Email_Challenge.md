# Email Challenge

This was the first of the challenges. It wasn't completely easy. I made a few silly little errors, such as uploading the wrong file, and getting the wrong IP address here and there.

From this challenge, I gained some invaluable skills in email header analysis using various tools which were unknown beforehand.

---
## The Tasks

#### Instructions
> As a level 1 SOC Analyst, you want to test your skills concerning email threats to gauge your ability to find security threats associated with phishing.

---
### Google Account
> Open Mozilla Thunderbird and find an email from Lana Banana with the title "This looks weird".
> Lana has attached an email that she received and something has triggered her spider-sense. Can you figure out what's going with the email by investigating it?

1. **What is the email's IP address of the origin?**
> `69.168.97.48`
> Opening up the attached email in *notepad* gave me access to all of the headers:
> ![Source IP](00-00-01-Source_IP.png)
	
2. **According to [ipinfo.io](https://ipinfo.io/), what is the the origin IP's company name?**
> `Synacor, Inc.`
> ![Company Name](00-00-02-Company_Name.png)

3. **According to [G Suite Toolbox](https://toolbox.googleapps.com/apps/messageheader/), what is the SPF status?**
> `softfail`
> Copy the email headers into the header analyser:
> ![SPF](00-00-03-SPF.png)

4. **Which URL stands out as malicious in this email?**
> `http://router-cf787a0e-21ef-49fd-9cf5-8f89eb091f8c.westeurope.cloudapp.azure.com`
> Taking a look at the message body, there are a number of these URLs littered all over the place:
> ![URL](00-00-04-URL.png)

5. **If you scan the malicious URL with [urlscan.io](https://urlscan.io/), what type of attack is this?**
> `phishing`
> The resulting image shows a Google login page:
>  ![Phishing](00-00-05-Phishing.png)


---
### Invoice Due Soon
> Your accounting department got an invoice from an unknown company, luckily they forwarded the email to you for double-checking before they opened the attachment.

1. **What is the IP address that delivered the email to your email server?**
> `153.153.62.99`
> This is the last IP address before it reaches the MX:
> ![Source IP](00-01-01-Source_IP.png)

2. **According to [MX Toolbox](https://mxtoolbox.com/EmailHeaders.aspx), what is the SPF Alignment result?**
> `Domain not found in SPF`
> ![SPF Alignment](00-01-02-SPF_Alignment.png)

3. **According to [VirusTotal](https://www.virustotal.com/gui/) in the categorization tags at the top of the results page, which scripting language is used by this malware?**
> `javascript`
> Initially, I uploaded the email and not the attachment. But when you upload the correct PDF attachment to *VirusTotal*, the results are shown below:
> ![Javascript](00-01-03-Javascript.png)


---
### Account Hacked
> Lana Banana got a threatening letter saying her account has been hacked. Help her determine is that really the case.

1. **What is the origin "*Received: from*" FQDN?**
> `serv1.youarehackzed.ga`
> ![FQDN](00-02-01-FQDN.png)


---
## Urgent
> Mark Bark got a Bitcoin demand letter from someone, you need to analyze the email to see if the threat is real.

1. **According to [MX Toolbox](https://mxtoolbox.com/EmailHeaders.aspx), how long did it take for this email to arrive?**
> `24364800 seconds`
> Pasting the headers into *MX Toolbox*, the delay is displayed at the top:
> ![Delay](00-03-01-Delay.png)

2. **What is the IP address that delivered the email to your email server?**
> `133.242.140.203`
> This is the last `Received` before it reached the MX server:
> ![IP](00-03-02-IP.png)


---
## FedEx Billing
> The accounting department got an email that looks like it came from FedEx. What are the signs that the FedEx Billing email is not what it appears to be?

1. **What is the first "Received: from" IP address of `Fedex_Billing.eml`?**
> `135.101.115.149`
> ![IP](00-04-01-IP.png)

2. **According to [MX Toolbox](https://mxtoolbox.com/SuperTool.aspx?) SuperTool, what is the actual FedEx mail server IP address?**
> `204.135.242.196`
> Using the *MX Toolbox* and searching for `fedex.com`, the below is returned:
> ![Real IP](00-04-02-Real_IP.png)

3. **If you scan the attached file with [VirusTotal](https://www.virustotal.com/gui/), which CVE is being exploited?**
> `CVE-2010-3333`
> ![CVE](00-04-03-CVE.png)


---
## Helpdesk Request
> Tim Tram forwarded you an email where the helpdesk appears to be asking his credentials for. Company policy is not to give out your credentials to anybody, thus Tim is suspicious.

1. **The attached `Helpdesk_Request.eml` email appears to come from helpdesk@contoso.com, what is the message *origin IP*?**
> `117.4.117.46`
> ![IP](00-05-01-IP.png)

2. **If you *reply to* this email, who will actually receive it?**
> `helpdesk@contozo.com`
> ![Reply To](00-05-02-Reply_To.png)

3. **According to [G Suite Toolbox](https://toolbox.googleapps.com/apps/messageheader/analyzeheader), what is the result of "*delivered after*"?**
> `7 hours`
> ![Delivered After](00-05-03-Delivered_After.png)


---
## New App Connected
> Joe King forwarded you an email from Microsoft that says something about a new app connection. Is this a legitimate email or should Joe worry?

1. **What is the first "*Received: from*" IP address of 'New_app_connected_to_your_Microsoft_account.eml'?**
> `65.52.110.208`
> ![Received From](00-06-01-Received_From.png)

2. **According to [ipinfo.io](https://ipinfo.io/), what is the the origin IP's *company name*?**
> `Microsoft Corporation`
> ![Microsoft](00-06-02-Microsoft.png)

3. **According to [MX Toolbox](https://mxtoolbox.com/Public/Tools/EmailHeaders.aspx), what is the result of *SPF Syntax Check*?**
> `The record is valid`
> ![SPF Syntax](00-06-03-SPF.png)

4. **According to [MX Toolbox](https://mxtoolbox.com/Public/Tools/EmailHeaders.aspx), what is the result of *DKIM Syntax Check*?**
> `The record is valid`
> ![DKIM Syntax](00-06-04-DKIM.png)
