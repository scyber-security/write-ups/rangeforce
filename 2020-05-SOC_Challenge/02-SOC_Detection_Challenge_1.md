# SOC Detection Challenge 1

---
## Tasks

#### Instructions
> You are working in Commensurate Technology as a SOC analyst.
>
> Commensurate Tech is using Windows Security (formerly Windows Defender) as the antivirus solution and the computers are configured to gather various events using Sysmon and Splunk forwarder in combination to send logs to a central Splunk server.
>
> Monthly Security Health Check & Compliance Report indicates that Windows Security has been disabled on the host windows10.
> You are tasked with making sure whether this issue is just a technical glitch or not. You are also provided with a list of possible indicators of compromise (IOCs) to aid in your intelligence gathering.
>
> You need to find out whether it was disabled for malicious purposes and if so, what was done during the time the computer was compromised.
> 
> You will investigate the possible compromise using Splunk SIEM solution.

---
### Restore Windows Security
> You have been notified that the Windows Security AntiSpyware module has been disabled on windows10.
>
> Since you have had issues with Windows Security before, you already have a procedure to re-enable it, along with some handy scripts in C:\Tools\.

This is a simple task of following the instructions.


---
### Search for IOCs
> You received an updated list of IOCs for today. Your daily process is to use Splunk to look for these IOCs and if any match in your environment.
>
> 1. 192.168.0.126
> 2. 192.168.0.157
> 3. 192.168.0.212
> 4. 192.168.0.214

1. **Which of the *IOCs* returned events?**
> `192.168.0.212`

###### Search Parameter
![Search Parameter](./images/02-01-01-Search_Pattern.png)

###### Destination IP
![Single IP](./images/02-01-01-Single_IP.png)


---
### Gather Intelligence
> Now that you have the IOC-related events in Splunk, you can pivot and sift through them.

1. **What is the filename of the malware stager?**
> `LaunchPad.bat`

Looking at the first event, the `ParentCommandLine` gives the answer to what the malware stager is:
![LaunchPad](./images/02-02-01-Launch_Pad.png)

2. **What type of startup method was used to execute the stager?**
> `All Users Startup`

The `C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp\` contains executables which will be run when the user log ins.

3. **What is the *full path* of the archive that was created for data exfiltration?**
> `C:\Windows\Temp\g0nna3XF1l7h15.tAR`

In the fourth event, the `CommandLine` gives the answer to the full path of the archive:
![Archive](./images/02-02-03-Archive.png)

4. **What *application* was used for the data exfiltration attempt?**
`scp.exe`

In the event following the previous question's, the `CommandLine` gives the answer to the application:
![Archive](./images/02-02-04-Application.png)

5. **What is the *password* for the new malicious user?**
> `password123@`

The following command line adds the new user:
![Archive](./images/02-02-05-Password.png)

6. **How was the *malicious user* added to a *group*? (Format: Full command)**
> `"C:\windows\system32\net.exe" localgroup Administrators Palware /add`


---
## Analyze Malware Stager
> After extracting all the necessary information from the IOCs you want to do dynamic analysis of the malware.
>
> The windows10 host is currently in a controlled environment. Windows Security has been restarted and its features are re-enabled. Knowing the malware stager location, you need to execute the stager and see what Windows Security will show.


To get the answer to the questions in this section, I executed the batch file and waited for the **Windows Security GUI** to show the new threats.
1. **What is the *severity* of the detection?**
> `severe`

2. **What is the name of the threat?**
> `PowerShell/Empire.A`

![Execute](./images/02-03-01-Execute.png)


---
## Delete the Stager
> Now that you have investigated the compromise, found the malicious events, and checked the detection in Windows Security, you need to follow your organization's policies around incident response and recovery. In most cases, this would include you writing an incident report.
>
> Per Commensurate Technology policy, since you identified what the malware is doing, you are going to delete the stager from disk on the system and clean it up. Your answer to the VTA questions are sufficient reporting for this incident.
