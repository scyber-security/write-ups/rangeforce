# SOC Challenge

This is the SOC challenge released by RangeForce from 14th May 2021, and run until 31st May 2021.

The challenge consisted of three challenges:

1. Windows - Email Challenge
2. Finding Patterns with Yara
3. SOC Detection Challenge 1

#### 1. Windows - Email Challenge
This challenge consisted of analysing the headers of some fishy looking emails using the trusty ol' *notepad* and online analysis tools.